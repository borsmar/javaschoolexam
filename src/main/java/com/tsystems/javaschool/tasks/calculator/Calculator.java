package com.tsystems.javaschool.tasks.calculator;
import java.util.Stack;

public class Calculator {

public static Boolean checking(String Expr){

        if(Expr.contains("(") && !Expr.contains(")")){
            return false;
        }

        if(Expr.contains(")") && !Expr.contains("(")){
            return false;
        }


        int a = Expr.length()-1;

        for(int i=0; i<Expr.length(); i++) {
            if(!(Expr.charAt(i) == '*'
                    || Expr.charAt(i) == '/'
                    || Expr.charAt(i) == '.'
                    || Expr.charAt(i) == '+'
                    || Expr.charAt(i) == '-'
                    || Expr.charAt(i) == '('
                    || Expr.charAt(i) == ')'||(Character.isDigit(Expr.charAt(i)))))

                return false;

        }

        if(Expr.charAt(a) == '.'
                || Expr.charAt(a) == '+'
                || Expr.charAt(a) == '-'
                || Expr.charAt(a) == '*'
                || Expr.charAt(a) == '/'
                || Expr.charAt(a) == '(' ){
            return false;
        }

        if(Expr.charAt(0) == '.' || Expr.charAt(0) == '*' || Expr.charAt(0) == '/' || Expr.charAt(0) == ')' ){
            return false;
        }

        for(int i=0; i<Expr.length(); i++) {

            if (Expr.charAt(0) == '-'){
                if (Expr.charAt(1) == '+' || Expr.charAt(1) == '-' || Expr.charAt(1) == '/' || Expr.charAt(1) == '*'){
                    return false;
                }
                continue;
            }

            if((Expr.charAt(i) == '+')||(Expr.charAt(i) == '-')||(Expr.charAt(i) == '*')||(Expr.charAt(i) == '/')) {
                if (Expr.charAt(i + 1) == '.'
                        || Expr.charAt(i + 1) == '+'
                        || Expr.charAt(i + 1) == '-'
                        || Expr.charAt(i + 1) == '*'
                        || Expr.charAt(i + 1) == '/'
                        || Expr.charAt(i - 1) == '.'
                        || Expr.charAt(i - 1) == '+'
                        || Expr.charAt(i - 1) == '-'
                        || Expr.charAt(i - 1) == '*'
                        || Expr.charAt(i - 1) == '/')
                    return false;
            }

            if(Expr.charAt(i) == '.' ) {
                if ((!Character.isDigit(Expr.charAt(i + 1)) || (!Character.isDigit(Expr.charAt(i - 1))))) {
                    return false;
                }
            }

            if (Expr.charAt(i)=='('){
                if (Expr.charAt(i + 1) == '*' || Expr.charAt(i + 1) == '/' || Expr.charAt(i + 1) == ')' ) {
                    return false;
                }

                if (i == 0){
                    continue;
                }

                if (Character.isDigit(Expr.charAt(i - 1)) || Expr.charAt(i - 1) == ')' ) {
                    return false;
                }
            }

            if (Expr.charAt(i)==')'){



                if (Expr.charAt(i - 1) == '*' || Expr.charAt(i - 1) == '/' || Expr.charAt(i - 1) == '(' ) {
                    return false;
                }

                if(i == a) {
                    continue;
                }

                if (Character.isDigit(Expr.charAt(i + 1)) || Expr.charAt(i + 1) == '(' ) {
                    return false;
                }
            }

        }

        return true;

    }



    public static String ExpressionToRPN(String Expr){



        String current = "";
        Stack<Character> stack = new Stack<>();

        int priority;
        for(int i = 0; i < Expr.length();i++ ){
            priority = getP(Expr.charAt(i));

            if(priority == 0) current+=Expr.charAt(i);
            if(priority == 1)stack.push(Expr.charAt(i));

            if(priority > 1){
                current+=' ';

                while(!stack.empty()){
                    if(getP(stack.peek()) >= priority) current+=stack.pop();
                    else break;
                }
                stack.push(Expr.charAt(i));
            }

            if(priority == -1){
                current+=' ';
                while(getP(stack.peek()) != 1) current+= stack.pop();

                stack.pop();
            }

        }
        while(!stack.empty())current+=stack.pop();

        return current;
    }

    private static String preparingExpression (String expression){
        String preparedExpression = new String();
        for(int token = 0; token < expression.length(); token++){
            char symbol = expression.charAt(token);
            if(symbol == '-'){
                if(token == 0)
                    preparedExpression += '0';
                else if (expression.charAt(token-1)=='(')
                    preparedExpression +='0';
            }
            preparedExpression+=symbol;
        }
        return  preparedExpression;
    }

    public static double RPNtoAnswer(String rpn){
        String operand = new String();
        Stack<Double> stack = new Stack<>();

        for(int i =0; i < rpn.length(); i++) {
            if (rpn.charAt(i) == ' ') continue;

            if (getP(rpn.charAt(i)) == 0) {
                while (rpn.charAt(i) != ' ' && getP(rpn.charAt(i)) == 0) {
                    operand += rpn.charAt(i++);
                    if (i == rpn.length()) break;
                }

                stack.push(Double.parseDouble(operand));
                operand = new String();
            }


            if(getP(rpn.charAt(i)) > 1){
                double a = stack.pop(), b = stack.pop();

                if(rpn.charAt(i) == '+')stack.push(b+a);
                if(rpn.charAt(i) == '-')stack.push(b-a);
                if(rpn.charAt(i) == '*')stack.push(b*a);
                if(rpn.charAt(i) == '/')stack.push(b/a);
            }

        }

        return stack.pop();
    }

    private static int getP(char token){
        if(token == '*' || token == '/')
            return 3;
        if(token == '+' || token == '-')
            return 2;
        if(token == '(')
            return 1;
        if(token == ')')
            return -1;
        else return 0;
    }




    public static String evaluate(String statement){
        if(!checking(statement))return null;



        double answer = RPNtoAnswer(ExpressionToRPN(preparingExpression(statement)));

        String s = Double.toString(answer);

        if(s.contains(".") && ((s.substring(s.indexOf("."), s.length()).length()) > 4)){
            return s.substring(0, s.indexOf(".")+5);
        }

        return s;

    }

}
